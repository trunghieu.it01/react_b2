import React, { Component } from 'react'
import Model from './Model'

export default class Display extends Component {
  render() {
    return (
      <div className='col-3'>
            <a onClick={()=>{this.props.handleChangeGlass(this.props.glass)}} href="#">
            
            <img src={this.props.glass.url} className="img-fluid w-50" alt="..." />
            </a>
            <h5>{this.props.glass.id}</h5>
        
      </div>
    )
  }
}
