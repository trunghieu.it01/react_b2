import React, { Component } from 'react'
import { dataGlasses } from './dataGlasses'
import Display from './Display'
import Model from './Model'

import model from './glassesImage/model.jpg'
export default class RenderWithMap extends Component {
  state={
    data: dataGlasses,
    detail: {}
 
  }
  handleChangeGlass =(glass)=>{
    console.log('glass: ', glass);
    
    return this.setState({
      detail: glass
    })
  }  
  renderGlasses=()=>{
        return this.state.data.map((item,index)=>{
           
            return <Display 
            handleChangeGlass = {this.handleChangeGlass}
            glass = {item}
            
            key={index}/>
        })
    }
    render() {
    return (
      <div className={`row container mt-5 mx-auto`} >
        {/* <div className='pb-5 position-relative'>
        <img className=' w-50' src={model} alt="" />
        <img className={`position-absolute  ${styles.image}`} src={this.state.detail.url} alt="" />
        <div className={`position-absolute ${styles.content}` }>
          <h3 className=' ps-3'>{this.state.detail.name}</h3>
          <h5 className='text-left ps-3 text-light'>{this.state.detail.desc}</h5>
        </div>
      </div> */}
      <Model detail={this.state.detail}/>
       {this.renderGlasses()} 
      </div>
    )
  }
}
